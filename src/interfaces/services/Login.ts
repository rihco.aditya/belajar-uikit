export type ssoType = 'google' | 'belajarid';

export interface ILoginSso {
  authorization_code?: string;
  type: ssoType;
}

export type LoginSsoResponse = {
  access_token: string;
  google_token: string;
  refresh_token: string;
};

export interface ILoginSsoResponse {
  status: string;
  data: LoginSsoResponse;
}

export interface ILoginSsoResponseMapped {
  accessToken: string;
  googleToken: string;
  refreshToken: string;
}

export interface ILoginManual {
  credential: string;
  pin: string;
}

export interface JwtPayload {
  exp: number;
  iat: number;
  iss: string;
  sub: string;
  ver: string;
}
