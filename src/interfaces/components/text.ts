import React from "react";

import type { ColorIndex } from "../colors";
import type { TypoIndex } from "../typo";

type align = "left" | "center" | "right";

export type wordBreakType =
  | "normal"
  | "break-all"
  | "keep-all"
  | "break-word"
  | "initial"
  | "inherit";

export interface StyledTextProps {
  type: TypoIndex;
  color?: ColorIndex;
  align?: align;
  link: boolean;
  withEllipsis?: boolean;
  width?: string;
  wordBreak?: wordBreakType;
}

export interface TextProps extends React.HTMLAttributes<HTMLElement> {
  type?: TypoIndex;
  children: React.ReactNode;
  color?: ColorIndex;
  className?: string;
  align?: align;
  link?: boolean;
  width?: string;
  withEllipsis?: boolean;
  wordBreak?: wordBreakType;
}
