import type { TagProps as AntdLabelProps } from "antd";
import type { LabelSizeIndex } from "../constants/labelSize";
import type { LabelVariantIndex } from "../constants/labelVariant";

export interface LabelProps extends AntdLabelProps {
  size?: LabelSizeIndex;
  type?: LabelVariantIndex;
}
