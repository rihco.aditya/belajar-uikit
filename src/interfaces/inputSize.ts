export type InputSizeType = "sm" | "md" | "lg";

export type InputSizeCollection = {
  height: string;
};

export interface InputSize {
  sm: InputSizeCollection;
  md: InputSizeCollection;
  lg: InputSizeCollection;
}
