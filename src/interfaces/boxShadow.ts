type BoxShadowCollection = {
  key: string;
  value: string;
};

interface BoxShadow {
  default: BoxShadowCollection;
  card: BoxShadowCollection;
  tooltip: BoxShadowCollection;
  overlay: BoxShadowCollection;
  primary: BoxShadowCollection;
  input: BoxShadowCollection;
}

export default BoxShadow;
