export type borderRadiusItem = string;

interface BorderRadius {
  sharp: borderRadiusItem;
  circle: borderRadiusItem;
  default: borderRadiusItem;
  label: borderRadiusItem;
}

export default BorderRadius;
