import { colors } from "./index";

import type { AvatarSize } from "../interfaces/constants/avatarSize";

const avatarSize: AvatarSize = {
  xs: { size: "24px", iconSize: 16 },
  sm: { size: "32px", iconSize: 20 },
  md: { size: "40px", iconSize: 24 },
  lg: { size: "48px", iconSize: 28 },
  xl: { size: "56px", iconSize: 32 },
  xxl: { size: "64px", iconSize: 36 },
  profileMd: {
    size: "96px",
    iconSize: 48,
    border: `solid ${colors.n0} 4px`,
    withShadow: true,
  },
  profileLg: {
    size: "160px",
    iconSize: 60,
    border: `solid ${colors.n0} 4px`,
    withShadow: true,
  },
};

export default avatarSize;
