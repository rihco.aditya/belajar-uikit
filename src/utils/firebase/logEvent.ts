import { logEvent as logEventFirebase, Analytics } from "firebase/analytics";
import firebaseInit from "./initialize";

let firebaseInstance: Analytics;

const logEvent = async (instance: Analytics, eventName = "", options = {}) => {
  if (firebaseInstance) {
    return logEventFirebase(instance || firebaseInstance, eventName, options);
  }
};

const initilizeFirebase = () => {
  if (!firebaseInstance) firebaseInstance = firebaseInit();
};

export default { initilizeFirebase, logEvent };
