import type { LoginSsoResponse } from "../../interfaces/services/Login";

const loginToJson = (data: LoginSsoResponse) => ({
  accessToken: data.access_token,
  googleToken: data.google_token,
  refreshToken: data.refresh_token,
});

export { loginToJson };
