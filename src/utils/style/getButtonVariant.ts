import { css } from "styled-components";

import { buttonVariant } from "../../constants/index";
import getBorderRadius from "utils/style/getBorderRadius";

import type { Variant } from "../../interfaces/buttonVariant";

const getState = (isLoading: boolean, isDisabled: boolean) => {
  if (isLoading && !isDisabled) {
    return "loading";
  }
  return "disabled";
};

const getBaseColor = (
  isLoading: boolean,
  isDisabled: boolean,
  type: Variant
) => {
  if (isLoading || isDisabled) {
    return buttonVariant[type].state[getState(isLoading, isDisabled)];
  }
  return buttonVariant[type].default;
};

const getButtonVariant = ({
  type,
  loading,
  disabled,
}: {
  type: Variant;
  loading: boolean;
  disabled: boolean;
}) => {
  return css`
    background-color: ${getBaseColor(loading, disabled, type).color};
    border: ${getBaseColor(loading, disabled, type).border};
    color: ${getBaseColor(loading, disabled, type).fontColor};
    ${getBorderRadius("default")};
    opacity: unset;

    &.ant-btn.ant-btn-loading {
      cursor: ${(disabled || loading) && "not-allowed"};
      background-color: ${getBaseColor(loading, disabled, type).color};
      border: ${getBaseColor(loading, disabled, type).border};

      .ant-btn-loading-icon {
        margin-right: 8px;
      }
    }

    :hover {
      background-color: ${buttonVariant[type].hover.color};
      border: ${buttonVariant[type].hover.border};
      color: ${buttonVariant[type].hover.fontColor};
    }

    :focus {
      box-shadow: ${buttonVariant[type].pressed.boxShadow};
      border: ${buttonVariant[type].pressed.border};
      color: ${buttonVariant[type].pressed.fontColor};
      background-color: ${buttonVariant[type].pressed.color};
    }

    &.ant-btn-primary[disabled],
    .ant-btn-primary[disabled]:active,
    .ant-btn-primary[disabled]:focus,
    .ant-btn-primary[disabled]:hover {
      background-color: ${getBaseColor(loading, disabled, type).color};
      border: ${getBaseColor(loading, disabled, type).border};
      color: ${buttonVariant[type].state[getState(loading, disabled)]
        .fontColor};
    }
  `;
};

const getButtonIconVariant = (type: Variant = "primary") => {
  return css`
    ${getBorderRadius("default")};

    background-color: ${getBaseColor(false, false, type).color};
    border: ${getBaseColor(false, false, type).border};
  `;
};

export { getButtonVariant, getButtonIconVariant };
