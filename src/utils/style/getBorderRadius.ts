import { css } from "styled-components";

import { borderRadius } from "../../constants/index";

import type { BorderRadiusType } from "../../interfaces/getBorderRadius";

const getBorderRadius = (type: BorderRadiusType = "default") => {
  return css`
    border-radius: ${borderRadius[type as BorderRadiusType]};
  `;
};

export default getBorderRadius;
