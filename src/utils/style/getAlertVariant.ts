import { css } from "styled-components";

import { alertVariant } from "../../constants/index";
import getBorderRadius from "../../utils/style/getBorderRadius";

import type { AlertType } from "../../interfaces/components/alert";

const getAlertVariant = ({ variant }: { variant: AlertType }) => {
  return css`
    background-color: ${alertVariant[variant].backgroundColor};
    color: ${alertVariant[variant].textColor};
    border: solid 1px ${alertVariant[variant].borderColor};
    ${getBorderRadius()}
  `;
};

export default getAlertVariant;
