import axios from "axios";
import Cookies from "js-cookie";
import jwtDecode from "jwt-decode";

import moment from "moment";
import { refreshingToken } from "../services/Login";
import { JwtPayload } from "../interfaces/services/Login";

let contextToken = "";
let contextRToken = "";

export const setToken = (accessToken: string) => {
  contextToken = accessToken;
};

export const setRefreshToken = (refreshToken: string) => {
  contextRToken = refreshToken;
};

const loginUrls = ["/user/v1.0/login/google", "/user/v1.0/login/basic"];

const customAxios = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_GATEWAY,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
    "X-Requested-With": "XMLHttpRequest",
    "App-ID": "sw",
    "Device-Type": "browser",
    "Device-ID": "123",
  },
});

customAxios.interceptors.request.use((config) => {
  const token = Cookies.get("accessToken");
  const usedToken = token || contextToken;
  config.headers = {
    ...config.headers,
    Authorization: `Bearer ${usedToken}`,
  };
  return config;
});

customAxios.interceptors.response.use(
  function (response) {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if (
      loginUrls.includes(originalRequest.url) &&
      error.response.status === 401
    ) {
      return Promise.reject(error);
    }
    // TODO: please update this response status to 401 when BE already give correct response status on token expired
    if (error.response.status !== 400 && !originalRequest._retry) {
      originalRequest._retry = true;
      const token = Cookies.get("accessToken");
      const rToken = Cookies.get("refreshToken");
      let usedToken = token || contextToken;
      const usedRToken = rToken || contextRToken;
      const now = moment().unix();
      let expiry: number = now;
      if (usedToken) {
        const decodeToken = jwtDecode<JwtPayload>(usedToken);
        expiry = decodeToken.exp - 3600;
      }
      if (expiry < now && usedRToken) {
        return refreshingToken(usedRToken).then((reNew) => {
          usedToken = reNew.accessToken;
          setToken(usedToken);
          setRefreshToken(reNew.refreshToken);
          Cookies.set("accessToken", usedToken);
          Cookies.set("refreshToken", reNew.refreshToken);
          Cookies.set("googleToken", reNew.googleToken);
          customAxios.defaults.headers.common.Authorization = `Bearer ${usedToken}`;
          return customAxios(originalRequest);
        });
      }
    }
    return Promise.reject(error);
  }
);

export default customAxios;
