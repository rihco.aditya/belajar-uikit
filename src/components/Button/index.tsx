import React from "react";

import { StyledButton } from "./styled";

import { BaseButtonProps } from "../../interfaces/components/button";

const Button: React.FC<BaseButtonProps> = ({
  size = "md",
  type = "primary",
  loading = false,
  isDisabled = false,
  onClick,
  block,
  children,
  prefix,
  suffix,
  ...restProps
}) => {
  return (
    <StyledButton
      type={type}
      size={size}
      loading={loading}
      onClick={onClick}
      disabled={isDisabled}
      block={block}
      {...restProps}
    >
      {prefix && !loading && prefix}
      {children}
      {suffix && suffix}
    </StyledButton>
  );
};
Button.defaultProps = {};
export default Button;
