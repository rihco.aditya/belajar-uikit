import { Avatar } from "antd";
import styled, { css, StyledComponent } from "styled-components";

import { colors, avatarSize } from "../../constants/index";
import getBoxShadow from "utils/style/getBoxShadow";

import type { AvatarProps } from "../../interfaces/components/avatar";
import type { AvatarIndex } from "../../interfaces/constants/avatarSize";

const getAvatarSizeValue = ({ size }: { size: AvatarIndex }) => css`
  width: ${avatarSize[size].size};
  height: ${avatarSize[size].size};
  border: ${avatarSize[size].border || "none"};
  box-shadow: ${avatarSize[size].withShadow ? getBoxShadow("default") : "none"};
`;

export const StyledAvatar: StyledComponent<any, any, AvatarProps> = styled(
  Avatar
)<AvatarProps>`
  ${getAvatarSizeValue}
  color: ${colors.p500};
  background-color: ${colors.p50};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const AvatarContainer = styled.div`
  display: inline-block;
`;
