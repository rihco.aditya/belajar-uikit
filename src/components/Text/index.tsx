import React from "react";

import { StyledText } from "./styled";

import { TextProps } from "../../interfaces/components/text";

const Text: React.FC<TextProps> = ({
  type = "bodyDefault16",
  children,
  className,
  color = "n900",
  align = "left",
  link = false,
  withEllipsis,
  width,
  ...restProps
}) => {
  return (
    <StyledText
      type={type}
      className={className}
      color={color}
      align={align}
      link={link}
      withEllipsis={withEllipsis}
      width={width}
      {...restProps}
    >
      {children}
    </StyledText>
  );
};

export default Text;
