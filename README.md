# Introduction

This is a collection of components, utility functions and config for gredu application

# Installation belajar-ui-kit

prerequisite

- yarn ^v1.22.11
- npm ^6.14.17
- node ^v14.17.6
  before to install ensure setup local registry to point out our gitlab registry

## NPM Command

```
echo @gredu-asia:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

then

```
npm install --save @gredu-asia/ui-labs
```

## Yarn Command

```
echo @gredu-asia:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

then

```
yarn add --save @gredu-asia/ui-labs
```

Maintainer
MIT © rihco.aditya
