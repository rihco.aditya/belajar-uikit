import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import dts from "rollup-plugin-dts";
import babel from "rollup-plugin-babel";

const packageJson = require("./package.json");
const getBabelOptions = ({ useESModules }) => ({
  exclude: "**/node_modules/**",
  extensions: [".ts", ".tsx", ".js", ".jsx"],
  runtimeHelpers: true,
  plugins: [["@babel/plugin-transform-runtime", { useESModules }]],
});

export default [
  {
    input: "src/index.ts",
    output: [
      {
        file: packageJson.main,
        format: "cjs",
      },
      {
        file: packageJson.module,
        format: "esm",
      },
    ],
    external: ["stream", "styled-components", "react", "react-dom"],
    globals: { "styled-components": "styled" },
    plugins: [
      resolve(),
      commonjs(),
      typescript({ tsconfig: "./tsconfig.json" }),
      babel(getBabelOptions({ useESModules: false })),
    ],
  },
  {
    input: "dist/esm/types/index.d.ts",
    output: [{ file: "dist/index.d.ts", format: "esm" }],
    plugins: [dts()],
  },
];
